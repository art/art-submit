#!/bin/bash
# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
# author : Tulay Cuhadar Donszelmann <tcuhadar@cern.ch>

if [ $# -ne 1 ]; then
    echo 'Usage: art-submit.sh local|grid'
    exit 1
fi

TYPE=$1
shift

MAX_RETRIES=4
SLEEP_TIME=600
TIMEOUT=36000

ART_NIGHTLY_RELEASE=${NIGHTLY_RELEASE}
ART_NIGHTLY_TAG=${NIGHTLY_TAG}
ART_PROJECT=${PROJECT}
ART_PLATFORM=${PLATFORM}

# add default packages and options in case of some branches and projects
# shellcheck disable=SC2086
DEFAULT_PACKAGES=$(./art-gitlab.py packages ${ART_NIGHTLY_RELEASE} ${ART_PROJECT} ${ART_PLATFORM} $TYPE)
retVal=$?
if [ $retVal -eq 2 ]; then
  # silently fail at the submit stage
  echo "${DEFAULT_PACKAGES}"
  exit 0
fi
if [ $retVal -eq 1 ]; then
  PACKAGES=${DEFAULT_PACKAGES}
  # RUN_ALL_TESTS=yes
fi

echo "art-submit: ATLAS setup"
mkdir tmp
mkdir "$HOME/.alrb"

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
# shellcheck disable=SC1091
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh || true
# lsetup -a testing asetup

asetup --version
if [[ $ART_PLATFORM == x86_64-centos7* ]]; then
   echo "<< asetup --version"
fi

ART_NIGHTLY_RELEASE_SHORT=${ART_NIGHTLY_RELEASE/-VAL-*/-VAL}

if [ "$TYPE" == "grid" ]; then
  echo "art-submit: RUCIO setup"
  export RUCIO_ACCOUNT=artprod
  lsetup panda
  echo ${RUCIO_ACCOUNT}
fi

# source /cvmfs/sft.cern.ch/lcg/views/LCG_96/${ART_PLATFORM}/setup.sh

# asetup needs to be retried a few times, sometimes its not ready yet
TRY=0
while [  $TRY -lt $MAX_RETRIES ]; do
    (( TRY=TRY+1 ))

    if [ -z "${NO_BUILD+x}" ]; then
      NO_BUILD=""
    else
      NO_BUILD="--no-build=${ART_NIGHTLY_TAG}"
      ART_NIGHTLY_TAG="latest"
    fi

    echo "art-submit: asetup try: $TRY"

    # ATTENTION: removing r in front of ART_NIGHTLY_TAG
    asetup --platform="${ART_PLATFORM}" "${ART_NIGHTLY_RELEASE_SHORT},${ART_NIGHTLY_TAG},${ART_PROJECT}" || true
    asetup --version
    # shellcheck disable=SC2154
    echo "${AtlasBuildBranch}"

    if [[ -n ${AtlasBuildBranch:?} ]]
    then
      if [ "$TYPE" == "grid" ]; then
      echo "art-submit: VOMS setup"
        lsetup "rucio -w" -a artprod || true
        # Try testing version of rucio
        # export ALRB_artVersion=testing
        # lsetup -f "rucio 1.21.4" -a artprod || true
	# user.artprod
	# voms-proxy-init --rfc -noregen -cert ./grid.proxy -voms atlas --valid 48:00
        voms-proxy-init --rfc -noregen -cert ./grid.proxy -voms atlas:/atlas/art/Role=production --valid 48:00
      fi

      echo "art-submit: xrootd setup"
      lsetup -f "xrootd 5.1.1"
      export PYTHONPATH=art/python:${PYTHONPATH}

      if [ -z "${RUN_ALL_TESTS+x}" ]; then RUN_ALL_TESTS=""; else RUN_ALL_TESTS="yes"; fi
      echo "art-submit: option --run-all-tests(${RUN_ALL_TESTS})"

      if [ "$TYPE" == "grid" ]; then
        echo "art-submit: art-clean"
        art/scripts/art-clean.py --eos --config=art-configuration.yml --delete "${ART_NIGHTLY_RELEASE_SHORT}" "${ART_PROJECT}" "${ART_PLATFORM}" || true &
        echo "art-submit: Sleep 60 seconds for art-clean"
        sleep 60

        if [ -z "${NO_COPY+x}" ]; then NO_COPY=""; else NO_COPY="yes"; fi
        echo "art-submit: option --no-copy(${NO_COPY})"
        if [ -z "${NO_ACTION+x}" ]; then NO_ACTION=""; else NO_ACTION="yes"; fi
        echo "art-submit: option --no-action(${NO_ACTION})"
        if [ -z "${PACKAGES+x}" ]; then PACKAGES=""; fi
        echo "art-submit: packages(${PACKAGES})"

        echo "art-submit: art submit"
        # shellcheck disable=SC2086
        art/scripts/art.py submit ${RUN_ALL_TESTS:+"--run-all-tests"} ${NO_COPY:+"--no-copy"} ${NO_BUILD} ${NO_ACTION:+"--no-action"} "${CI_PIPELINE_ID}" ${PACKAGES:+${PACKAGES}}
      fi
      if [ "$TYPE" == "local" ]; then
        OUTPUT_DIR="root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/data-art/local-output"

        echo "art-submit: art-clean"
        art/scripts/art-clean.py -v --eos --release --base-dir="${OUTPUT_DIR}" --delete "${ART_NIGHTLY_RELEASE_SHORT}" "${ART_PROJECT}" "${ART_PLATFORM}" || true &
        echo "art-submit: Sleep 60 seconds for art-clean"
        sleep 60

        if [ -z "${TIMEOUT+x}" ]; then TIMEOUT=""; else TIMEOUT="--timeout=${TIMEOUT}"; fi
        echo "art-submit: timeout(${TIMEOUT})"

        echo "art-submit: art run"
        art/scripts/art.py run ${RUN_ALL_TESTS:+"--run-all-tests"} ${TIMEOUT} --max-jobs=4 "/cvmfs/atlas-nightlies.cern.ch/repo/sw/${ART_NIGHTLY_RELEASE}_${ART_PROJECT}_${ART_PLATFORM}/${ART_NIGHTLY_TAG}/${ART_PROJECT}"/*/"InstallArea/${ART_PLATFORM}/src" "./art-local/${AtlasBuildBranch:?}/${ART_PROJECT}/${ART_PLATFORM}/${ART_NIGHTLY_TAG}" --copy "${OUTPUT_DIR}/${ART_NIGHTLY_RELEASE}/${ART_PROJECT}/${ART_PLATFORM}/${ART_NIGHTLY_TAG}"
      fi
      exit 0
    fi
    echo "art-submit: sleep ${SLEEP_TIME}"
    sleep "${SLEEP_TIME}"
done
exit 1
