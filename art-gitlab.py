#!/usr/bin/env python3
# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
"""
ART-gitlab - ATLAS Release Tester (submit command).

Usage:
  art-gitlab.py included            [-v]  <nightly_release> <project> <platform>
  art-gitlab.py wait_for            [-v --no-wait]  <nightly_release> <project> <platform> <nightly_tag>
  art-gitlab.py packages            [-v]  <nightly_release> <project> <platform> [<type>]

Options:
  -h --help         Show this screen.
  --no-wait         Do not wait an extra 30 minutes after finding the logfile of the release
  --version         Show version.
  -v, --verbose     Show details.

Sub-commands:
  included          Check if a release and platform is included
  wait_for          Wait for the release to be available
  packages          Returns 0 and 1 to abort. An optional list of packages is printed to stdout

Arguments:
  nightly_release   Name of the nightly release (e.g. 21.0)
  nightly_tag       Nightly tag (e.g. 2017-02-26T2119)
  platform          Platform (e.g. x86_64-slc6-gcc62-opt)
  project           Name of the project (e.g. Athena)
  type              local or grid
"""
from __future__ import print_function

__author__ = "Tulay Cuhadar Donszelmann <tcuhadar@cern.ch>"
__version__ = '1.4.12'

import datetime
import fnmatch
import glob
import os
import re
import sys
import time
import yaml

import requests

from docopt_dispatch import dispatch


@dispatch.on('included')
def included(nightly_release, project, platform, **kwargs):
    """TBD."""
    platform_pattern = '*-*-*-opt'
    if not fnmatch.fnmatch(platform, platform_pattern):
        print('Excluding ' + 'all' + ' for ' + nightly_release + ' project ' + project + ' on ' + platform)
        exit_fail(1, 'excluded')

    if nightly_release == 'const-master' and project == 'Athena':
        print('Excluding ' + 'all' + ' for ' + nightly_release + ' project ' + project + ' on ' + platform)
        exit_fail(1, 'excluded')

    exit_ok('included')


@dispatch.on('wait_for')
def wait_for(nightly_release, project, platform, nightly_tag, **kwargs):
    """TBD."""
    branch = nightly_release

    branches_no_wait = []
    should_wait = branch not in branches_no_wait and not kwargs.get('no_wait')
    if not should_wait:
        exit_ok("not waiting for CVMFS sync")

    # 1. Wait for the nightly to show up at the CERN stratum
    # LCG nightlies do not have an ayum log as they are not installed from RPMs,
    # so they get handled differently
    lcg_branches = ['main--dev3LCG', 'main--dev4LCG']

    cvmfs_directory = '/cvmfs/atlas-nightlies.cern.ch/repo/sw'
    nd = _nightly_dir(cvmfs_directory, branch, project, platform, nightly_tag)
    wait_fn = wait_for_lcg_nightly if branch in lcg_branches else wait_for_normal_nightly
    wait_fn(nd, max_wait_hours=3)

    # 2. Wait for the nightly to show up at the remote strata
    wait_for_remote_cvmfs_sync(max_wait_hours=12)
    exit_ok()


def wait_for_lcg_nightly(nightly_dir, max_wait_hours=None):
    nightly = os.path.basename(os.path.dirname(nightly_dir))
    fields = nightly.split("_", 2)
    if len(fields) != 3:
        print(f"Unexpected nightly directory name: {nightly}")
        exit_fail(3, "unexpected nightly directory name")

    project = fields[1]

    subdirs = [
        project,
        project + "Externals"
    ]
    if project == "Athena":
        subdirs.extend(["tdaq", "tdaq-common"])

    absent_paths = [os.path.join(nightly_dir, subdir) for subdir in subdirs]

    is_timed_out = timer(hours=max_wait_hours)

    status("waiting for lcg nightly")
    while not is_timed_out() and absent_paths:
        for path in absent_paths[:]:
            if os.path.exists(path):
                absent_paths.remove(path)
        if absent_paths:
            status("waiting for lcg nightly")
            time.sleep(60)

    if absent_paths:
        missing = "\n".join(absent_paths)
        print(f"Missing LCG sub-dirs:\n {missing}")
        exit_fail(2, "failed to find all required LCG nightly sub-dirs")

    status("lcg nightly found ok")


def wait_for_normal_nightly(nightly_dir, max_wait_hours=None):
    # look and wait for ayum file
    nightly_tag = os.path.basename(nightly_dir)
    nightly = os.path.basename(os.path.dirname(nightly_dir))
    fields = nightly.split("_", 2)
    if len(fields) != 3:
        print(f"Unexpected nightly directory name: {nightly}")
        exit_fail(3, "unexpected nightly directory name")

    branch, project, platform = fields
    log_patt = ayum_log_patt(branch, project, platform, nightly_tag)
    ayum_log = _wait_for_ayum_log(os.path.join(nightly_dir, log_patt), max_wait_hours)
    if not file_contains(ayum_log, search_term="looks to have been successful"):
        exit_fail(2, "ayum log not ok")
    status("ayum log and nightly found ok")


def _wait_for_ayum_log(log, max_wait_hours=None):
    """Wait for ayum log to appear on CERN CVMFS. If max_wait_hours is None, wait forever."""
    status(f"waiting for ayum log: {log}")
    is_timed_out = timer(max_wait_hours)

    while not is_timed_out():
        matches = glob.glob(log)
        if matches:
            return sorted(matches, key=os.path.getctime, reverse=True)[0]

        status("waiting for ayum log")
        time.sleep(60)

    exit_fail(1, "ayum log not found")


def wait_for_remote_cvmfs_sync(max_wait_hours=None):
    """Wait for remote CVMFS sites to sync. If max_wait_hours is None, wait forever."""
    status("waiting for cvmfs remote sync")
    is_timed_out = timer(max_wait_hours)
    cern_rev = get_cvmfs_revision('cern')
    print(f"Require remote CVMFS revision >= CERN ({cern_rev})")

    unsynced_sites = ['bnl']
    while not is_timed_out():
        for site in unsynced_sites[:]:
            rev = get_cvmfs_revision(site)
            if rev >= cern_rev:
                print(f'{site.upper()} is at revision {rev}, need >= {cern_rev}. All ok.')
                unsynced_sites.remove(site)
            else:
                print(f'{site.upper()} is at revision {rev}, need >= {cern_rev}. Will wait.')

        if not unsynced_sites:
            print('Remote sites are synced, can proceed.')
            return

        # let's wait a bit
        wait_mins = 10
        print(f"Waiting an extra {wait_mins} mins for remote sync", flush=True)
        time.sleep(wait_mins * 60)

    # If we are here, we timed out
    print(f"Time out, sites not synced: {', '.join(unsynced_sites)}", flush=True)
    exit_fail(2, "remote sites not synced")


def get_cvmfs_revision(name):
    try:
        url = {
            'cern': 'http://cvmfs-stratum-one.cern.ch/cvmfs/atlas-nightlies.cern.ch/.cvmfspublished',
            'bnl': 'http://cvmfs.sdcc.bnl.gov/cvmfs/atlas-nightlies.cern.ch/.cvmfspublished',
            'kek': 'http://cvmfs-stratum-one.cc.kek.jp/cvmfs/atlas-nightlies.cern.ch/.cvmfspublished',
        }[name]
    except KeyError:
        print(f"Unknown CVMFS stratum: {name}")
        return 0

    resp = requests.get(url)
    if not resp.ok:
        print("Unable to fetch CVMFS revision for", name, ", got HTTP code", resp.status_code)
        return 0

    matches = re.findall('S(\\d+)', resp.text)
    if not matches:
        print("Response from CVMFS stratum", name, "did not include the revision number")
        return 0

    return int(matches[0])


def matched(entry, nightly_release, project, platform):
    """TBD."""
    if 'branches' in entry:
        for branch in entry['branches']:
            if nightly_release == branch:
                if 'projects' in entry:
                    for p in entry['projects']:
                        if project == p:
                            # Branch and Project match
                            return True
                else:
                    # Branch matched, no projects
                    return True
    else:
        # No branches
        if 'projects' in entry:
            for p in entry['projects']:
                if project == p:
                    # No branches, project matched
                    return True
    # No branches, no projects
    return False


@dispatch.on('packages')
def packages(nightly_release, project, platform, type=None, **kwargs):
    """TBD."""
    yml_file = os.path.join('.', 'art-submit.yml')
    if os.path.isfile(yml_file):
        with open(yml_file, 'r') as stream:
            try:
                config = yaml.safe_load(stream)
                for entry in config:
                    if matched(entry, nightly_release, project, platform):
                        if 'runs_on' in entry:
                            if not type:
                                # needs to run, but do not know where
                                # normally called from an early stage in gitlab
                                # may be excluded later on when type is known
                                exit_ok()
                            elif type in entry['runs_on']:
                                # run these with specified packages if any
                                if 'packages' in entry:
                                    print(' '.join((entry['packages'])))
                                exit_fail(1)
                            else:
                                # not in demanded type
                                print('Excluding ' + type + ' for ' + nightly_release + ' project ' + project + ' on ' + platform)
                                exit_fail(2, 'excluded')
                        else:
                            # no type specified, no need to run
                            print('Excluding local/grid for ' + nightly_release + ' project ' + project + ' on ' + platform)
                            exit_fail(2, 'excluded')
            except yaml.YAMLError as exc:
                print(exc)
                exit_fail(2)
    exit_ok()


def file_contains(path, search_term=None):
    """Check if the given file path contains the search term."""
    if not search_term:
        return False

    print(f"Grepping {path} for {search_term}")

    with open(path) as fd:
        for line in fd:
            if search_term in line:
                return True

    return False


def exit_ok(msg=None):
    status(msg)
    sys.exit(0)


def exit_fail(ec, msg=None):
    status(msg)
    sys.exit(ec)


def status(msg):
    if msg:
        print(f"art-status: {msg}", flush=True)


def get_config():
    """Retrieve dictionary of ART configuration file."""
    config_file = open("art-configuration.yml", "r")
    config = yaml.safe_load(config_file)
    config_file.close()
    return config


def _nightly_dir(basedir, branch, project, platform, nightly_tag):
    nightly_name = "_".join((branch, project, platform))
    return os.path.join(basedir, nightly_name, nightly_tag)


def ayum_log_patt(branch, project, platform, nightly_tag):
    # The * is the epoch field, for ex:
    # main__Athena__x86_64-el9-gcc13-opt__2023-05-29T2101__1685425623.ayum.log
    ayum_log = "__".join((branch, project, platform, nightly_tag, "*"))
    return f"{ayum_log}.ayum.log"


def timer(hours=None):
    start = datetime.datetime.now()
    end = None if hours is None else start + datetime.timedelta(hours=hours)
    print(f"Timer started for {hours} hours, ends: {end}")

    def time_up():
        now = datetime.datetime.now()
        return end is not None and now >= end
    return time_up


if __name__ == '__main__':
    dispatch(__doc__, version=os.path.splitext(os.path.basename(__file__))[0] + ' ' + __version__)
